import React,{useState} from 'react';
import { View , Text ,TouchableOpacity} from 'react-native'

const App= () => {
  const [time,setTime] = useState(0)
  const [ref,setRef] = useState(undefined)
  const [time2,setTime2] = useState(0)
  const [ref2,setRef2] = useState(undefined)
  const [time3,setTime3] = useState(0)
  const [ref3,setRef3] = useState(undefined)
  const [time4,setTime4] = useState(0)
  const [ref4,setRef4] = useState(undefined)

  return (
    <View style={{flex:1,justifyContent:'center',backgroundColor:'#eee'}}>
      <View>
        
        <View style={{flexDirection:'row-reverse',justifyContent:'space-around',backgroundColor:'#fff',borderRadius:18,elevation:10,margin:8}}>

          <View style={{flex:1,justifyContent:'center'}}>
            <Text style={{textAlign:'center',fontFamily:'IRANSansMobile'}}>1/3 هجومی</Text>
            <Text style={{textAlign:'center',fontFamily:'IRANSansMobile',fontSize:28,margin:8}}>{time!==0?('0' + Math.floor(time/600)).slice(-2) +':'+('0' + Math.floor(time/10)%60).slice(-2) +':'+ Math.floor(time%10):'00:00:0'}</Text>
            <TouchableOpacity onPress={()=>{
              setRef2(clearInterval(ref2))
              setRef3(clearInterval(ref3))
              setRef4(clearInterval(ref4))
              ref===undefined?
              setRef(setInterval(()=>{
                setTime(prev=>prev+1)
              },100))
            
              :setRef(clearInterval(ref))
            }}>
              <View style={[{height:36,margin:8,borderRadius:8,justifyContent:'center'},ref===undefined?{backgroundColor:'#20a314'}:{backgroundColor:'#b02e17'}]}>
                <Text style={{textAlign:'center',fontFamily:'IRANSansMobile',color:'#fff'}}>
                  {ref===undefined?'شروع':'توقف'}
                </Text>
              </View>
            </TouchableOpacity>
          </View>

          <Text style={{alignSelf:'center',textAlign:'center',fontFamily:'IRANSansMobile',fontSize:16,transform:[{rotate:'-90deg'}]}}>میزبان</Text>

          <View style={{flex:1,justifyContent:'center'}}>
            <Text style={{textAlign:'center',fontFamily:'IRANSansMobile'}}>سایر مناطق</Text>
            <Text style={{textAlign:'center',fontFamily:'IRANSansMobile',fontSize:28,margin:8}}>{time2!==0?('0' + Math.floor(time2/600)).slice(-2) +':'+('0' + Math.floor(time2/10)%60).slice(-2) +':'+ Math.floor(time2%10):'00:00:0'}</Text>
            <TouchableOpacity onPress={()=>{
              setRef(clearInterval(ref))
              setRef3(clearInterval(ref3))
              setRef4(clearInterval(ref4))
              ref2===undefined?
              setRef2(setInterval(()=>{
                  setTime2(prev=>prev+1)
                },100)
              )
              :setRef2(clearInterval(ref2))
            }}>
              <View style={[{height:36,margin:8,borderRadius:8,justifyContent:'center'},ref2===undefined?{backgroundColor:'#20a314'}:{backgroundColor:'#b02e17'}]}>
                <Text style={{textAlign:'center',fontFamily:'IRANSansMobile',color:'#fff'}}>
                  {ref2===undefined?'شروع':'توقف'}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>


      <View>
      
        <View style={{flexDirection:'row-reverse',justifyContent:'space-around',borderRadius:18,backgroundColor:'#fff',elevation:10,margin:8}}>
        <View style={{flex:1,justifyContent:'center'}}>
            <Text style={{textAlign:'center',fontFamily:'IRANSansMobile'}}>1/3 هجومی</Text>
            <Text style={{textAlign:'center',fontFamily:'IRANSansMobile',fontSize:28,margin:8}}>{time3!==0?('0' + Math.floor(time3/600)).slice(-2) +':'+('0' + Math.floor(time3/10)%60).slice(-2) +':'+ Math.floor(time3%10):'00:00:0'}</Text>
            <TouchableOpacity onPress={()=>{
              setRef2(clearInterval(ref2))
              setRef(clearInterval(ref))
              setRef4(clearInterval(ref4))
              ref3===undefined?
              setRef3(setInterval(()=>{
                setTime3(prev=>prev+1)
              },100))
            
              :setRef3(clearInterval(ref3))
            }}>
              <View style={[{height:36,margin:8,borderRadius:8,justifyContent:'center'},ref3===undefined?{backgroundColor:'#20a314'}:{backgroundColor:'#b02e17'}]}>
                <Text style={{textAlign:'center',fontFamily:'IRANSansMobile',color:'#fff'}}>
                  {ref3===undefined?'شروع':'توقف'}
                </Text>
              </View>
            </TouchableOpacity>
          </View>

          <Text style={{alignSelf:'center',textAlign:'center',fontFamily:'IRANSansMobile',fontSize:16,transform:[{rotate:'-90deg'}]}}>مهمان</Text>


          <View style={{flex:1,justifyContent:'center'}}>
            <Text style={{textAlign:'center',fontFamily:'IRANSansMobile'}}>سایر مناطق</Text>
            <Text style={{textAlign:'center',fontFamily:'IRANSansMobile',fontSize:28,margin:8}}>{time4!==0?('0' + Math.floor(time4/600)).slice(-2) +':'+('0' + Math.floor(time4/10)%60).slice(-2) +':'+ Math.floor(time4%10):'00:00:0'}</Text>
            <TouchableOpacity onPress={()=>{
              setRef(clearInterval(ref))
              setRef3(clearInterval(ref3))
              setRef2(clearInterval(ref2))
              ref4===undefined?
              setRef4(setInterval(()=>{
                  setTime4(prev=>prev+1)
                },100)
              )
              :setRef4(clearInterval(ref4))
            }}>
              <View style={[{height:36,margin:8,borderRadius:8,justifyContent:'center'},ref4===undefined?{backgroundColor:'#20a314'}:{backgroundColor:'#b02e17'}]}>
                <Text style={{textAlign:'center',fontFamily:'IRANSansMobile',color:'#fff'}}>
                  {ref4===undefined?'شروع':'توقف'}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
          </View>
      </View>

      <View style={{flexDirection:'row-reverse'}}>
        <TouchableOpacity style={{flex:1}} onPress={()=>{
          setTime(0)
          setTime2(0)
          setRef(clearInterval(ref))
          setRef2(clearInterval(ref2))
          }}>
            <View style={{height:36,margin:8,borderRadius:8,justifyContent:'center',backgroundColor:'#d95c0f'}}>
              <Text style={{textAlign:'center',fontFamily:'IRANSansMobile',color:'#fff'}}>
                ریست میزبان
              </Text>
            </View>
        </TouchableOpacity>
        <TouchableOpacity style={{flex:1}} onPress={()=>{
          setTime3(0)
          setTime4(0)
          setRef3(clearInterval(ref3))
          setRef4(clearInterval(ref4))
          }}>
            <View style={{height:36,margin:8,borderRadius:8,justifyContent:'center',backgroundColor:'#d95c0f'}}>
              <Text style={{textAlign:'center',fontFamily:'IRANSansMobile',color:'#fff'}}>
                ریست مهمان
              </Text>
            </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default App;
